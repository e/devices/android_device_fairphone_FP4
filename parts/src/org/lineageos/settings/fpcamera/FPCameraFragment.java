/*
 * Copyright (C) 2023-2024 The LineageOS Project
 * SPDX-License-Identifier: Apache-2.0
 */

package org.lineageos.settings.fpcamera;

import android.os.Bundle;

import androidx.preference.Preference;
import androidx.preference.PreferenceFragment;
import androidx.preference.SwitchPreference;

import org.lineageos.settings.R;

import java.io.IOException;

public class FPCameraFragment extends PreferenceFragment implements Preference.OnPreferenceChangeListener {

    private static final String PREF_FP_CAMERA = "fp_camera_pref";

    private SwitchPreference mFPCameraPref;

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        addPreferencesFromResource(R.xml.fp_camera_settings);

        mFPCameraPref = findPreference(PREF_FP_CAMERA);
        mFPCameraPref.setOnPreferenceChangeListener(this);
    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object newValue) {
        if (preference == mFPCameraPref) {
            boolean value = (Boolean) newValue;
            return FPCameraUtils.setApplicationEnabledSetting(getActivity(), value);
        }
        return false;
    }

    @Override
    public void onStop() {
        super.onStop();
    }
}
